<?php

include_once "header.php";

?>
    <body>
    <?php include_once "nav.html";?>

        <div class="jumbotron">
            <div class="container">
                <h1 style="font-family:Megrim; font-size:100px">NMU ACM</h1>
                <p>Association for Computing Machinery</p>
            </div>
        </div>
        <div class="container">
            <div class="row text-center">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <h2>Current Project</h2>
                    <div class="well"><a href="https://git.benharris.ch/acm/jsboy">JSBOY</a> - A GameBoy emulator written in JavaScript</div>
                    <div class="well"><a href="https://git.benharris.ch/snippets/2">More project ideas</a></div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <h2>Meetings</h2>
                    <div class="well">Meetings are Fridays at 4PM in the CS Lab</div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <h2>More Info</h2>
                    <div class="well">Please submit a merge request for this site at the
                    <a href="https://git.benharris.ch/acm/site">git repo</a></div>
                    <div class="well">Contact <a href="mailto:benharri@nmu.edu">Ben Harris</a> for more info</div>
                </div>
            </div>
        </div>
    </body>
<?php include_once "footer.php";